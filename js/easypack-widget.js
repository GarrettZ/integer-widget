/**
 * EasyPack Widget
 *
 * Simple togglable widget for recruitment purposes.
 *
 * @author Przemysław Włodek
 */

// send console information, that SDK has been initialized
console.log('SDK Initialize');

var easyPack = {
    // API address
    _easyPackAPIUrl : 'https://api-pl.easypack24.net/v4/machines?type=0',
    // METHOD to log every widget initialization
    notify : function() { console.log('Widget Initialize'); },
    // METHOD to bind widget to DOM element
    listWidget : function(id){

        this.notify();

        if(!this.cachedMachinesAddressList) {
            // if cache is empty, then fetch machines list from API
            this.fetchMachinesList(id);
        } else {
            // if not then turn into main _init process
            this._init(id);
        }
    },
    // METHOD to fetch machines list from EasyPack API
    // takes ID of DOM element on which widget will be initialized
    fetchMachinesList : function(callbackID){

        // throw error which stops process, when ID is undefined
        if(typeof callbackID === 'undefined'){
            throw 'ERROR: Please define DOM element ID to initialize widget.';
        }

        var that = this,
            http_request = false;

        // strongly inspired by Mozilla MDN solution
        if (window.XMLHttpRequest) { // Mozilla, Safari,...
            http_request = new XMLHttpRequest();
            if (http_request.overrideMimeType) {
                http_request.overrideMimeType('application/json');
            }
        } else if (window.ActiveXObject) { // IE
            try {
                http_request = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    http_request = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) {}
            }
        }

        if (!http_request) {
            alert('There is a problem with data request to EasyPack API, please try again later.');
            return false;
        }

        http_request.onreadystatechange = function() {
            if (http_request.readyState == 4 && http_request.status == 200) {
                var data = JSON.parse(http_request.responseText);
                // fill cache with fetched and parsed data
                that.cachedMachinesAddressList = that._parseMachinesAddresses(data);
                // initialize widget
                that._init(callbackID);
            }
        };

        http_request.open('GET', that._easyPackAPIUrl, true);
        http_request.send();
    },
    // PRIVATE METHOD to parse data from EasyPack API
    _parseMachinesAddresses : function(data){

        var machines = data._embedded.machines,
            machinesAddresses = new Array();

        // parse address string from machines data
        for(var i = 0; i < machines.length; i++){
            machinesAddresses.push( machines[i].address_str );
        }

        return machinesAddresses;
    },
    // PRIVATE METHOD initialize method invoked with listWidget function
    // its main process if we got cachedMachinesAddressList
    _init : function (id) {

        var widgetPlaceholder = document.getElementById(id),
            machinesList = this.cachedMachinesAddressList,
            machinesListDOM = document.createElement('ul');

        machinesListDOM.setAttribute('class', 'easypack-widget');

        // this iteration needs many improvements, but in this case
        // I can't spend more time to fix this. Unfortunately I was more at jQuery than Vanilla JS
        for(var i = 0; i < machinesList.length; i++){
            var machineItem = document.createElement('li'),
                addr = document.createTextNode(machinesList[i]);

            machineItem.setAttribute('class', 'machine');
            machineItem.appendChild(addr);
            machinesListDOM.appendChild(machineItem);
        }

        machinesListDOM.addEventListener('click', function() {
            // I know that classlist is not best solution for IE / old Androids
            // but it's the cleanest solution I could wrote in this time

            // toggle menu expand behavior
            this.classList.toggle('active');
            // no metter what - always start from top of list
            this.scrollTop = 0;
        }, false);

        // easy solution to clear node if widget was initialized before
        // I could write an error or something...
        widgetPlaceholder.innerHTML = '';
        widgetPlaceholder.appendChild(machinesListDOM);
    }
};

// I hope you're still there. Thanks